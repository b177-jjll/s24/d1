// Query Operators

// Comparison Query Operators
// $gt and $gte (Greater than/ Greater than or equal to)

// Find users with an age greater than 50
db.users.find({
	age : {
		$gt : 50
	}
})

db.users.find({
	age : {
		$gte : 50
	}
})

// $lt and $lte (Less than/Less than or equal to)

// Find users with an age lower than 50
db.users.find({
	age : {
		$lt : 50
	}
})

db.users.find({
	age : {
		$lte : 50
	}
})

// Find users with an age that is NOT equal to 82
db.users.find({
	age : {
		$ne : 82
	}
})

// Find users whose last names are either "Hawking" OR "Doe"
db.users.find({
	lastName : {
		$in : ["Hawking", "Doe"]
	}
})

// Find users whose courses including "HTML" OR "React"
db.users.find({
	courses : {
		$in : ["HTML", "React"]
	}
})

// Logical Query Operators

// $or operator
db.users.find({
	$or : [
		{
			firstName : "Neil"
		},
		{
			age : 21
		}
	]
})

// $and operator
db.users.find({
	$and : [
		{
			age : {$ne : 82}
		},
		{
			age : {$ne : 76}
		}
	]
})